'use strict';


var response = require('./../config/response');
var connection = require('./../config/db');

async function getLast(faskesid) {
    console.log(faskesid)
    let last = await connection('table_pasien_database').where('faskesid', faskesid).orderBy('id', 'desc').limit(1)
    console.log(last.length)
    if (last.length !== 0) {
        let rmd = last[0].rm
        let rmautoinc = rmd.substr(11, 15);
        console.log(rmautoinc);
        let nomor = parseInt(rmautoinc) + 1;
        if (nomor < 10) {
            let fixnomor = '000' + nomor;
           return fixnomor
        } else if (nomor >= 10 && nomor < 100) {
            let fixnomor = '00' + nomor;
           return fixnomor
        } else if(nomor >= 100 && nomor < 1000){
            let fixnomor = '0' + nomor;
            return fixnomor
        }else{
           let fixnomor = nomor;
           return fixnomor
        }
    } else {
       let  fixnomor = '0001';
       return fixnomor
    }
}


async function getCharNumb(namakk){
    let firstchar =  await namakk.substr(0,1)
    let upfirstchar = await firstchar.toUpperCase() //mengubah ke huruf besar
    let charNumb = await upfirstchar.charCodeAt() // mengubah ke ASCII
    return charNumb
}

exports.users = async function (req, res) {
    try {
        let pasien = await connection('table_pasien_database');
        res.json(pasien)
    } catch (e) {
        console.log(e);
    }
}

exports.userbyid = async function (req, res) {
    try {
        let pasienbyid = await connection('table_pasien_database').where('rm', req.params.id)
        console.log(pasienbyid[0].id)
        res.json(pasienbyid)
    } catch (e) {

    }
}

exports.pasiencreate = async function (req, res) {
    let nik = req.body.nik;
    let bpjs = req.body.bpjs;
    let old_rm = req.body.old_rm;
    let nama = req.body.nama;
    let jenis_kelamin = req.body.jenis_kelamin;
    let alamat = req.body.alamat;
    let tempat_lahir = req.body.tempat_lahir;
    let tanggal_lahir = req.body.tanggal_lahir;
    let no_hp = req.body.no_hp;
    let nama_kepala_keluarga = req.body.nama_kepala_keluarga;
    let status = req.body.status;
    let pekerjaan = req.body.pekerjaan;
    let agama = req.body.agama;
    let faskesid = req.body.kodefaskes

    let lastId = await getLast(faskesid);
    let numbChar = await getCharNumb(nama_kepala_keluarga);
    let fixrm = faskesid + numbChar + lastId;
    console.log(lastId+"/"+numbChar)
    try {
        let insert = await connection('table_pasien_database').insert({
            "nik": nik,
            "bpjs": bpjs,
            "rm": fixrm,
            "old_rm": old_rm,
            "nama": nama,
            "jenis_kelamin": jenis_kelamin,
            "agama": agama,
            "pekerjaan": pekerjaan,
            "tempat_lahir": tempat_lahir,
            "tanggal_lahir": tanggal_lahir,
            "nama_kepala_keluarga": nama_kepala_keluarga,
            "status": 'status',
            "no_hp": no_hp,
            "faskesid" : faskesid,
            "alamat": alamat,
        })
        res.json({ "status": "sukses" })
    } catch (e) {
        console.log(e)
    }

}

exports.index = function (req, res) {
    response.ok('Response Oke', res)
}

exports.icd = async function (req, res) {
    try {
        let icd = await connection('table_icd');
        res.json(icd)
    } catch (e) {
        console.log(e);
    }
}

exports.obat = async function (req, res) {
    try {
        let obat = await connection('table_obat');
        res.json(obat)
    } catch (e) {
        console.log(e);
    }
}

exports.jadwal = async function (req, res) {
    try {
        let jadwal = await connection('table_jadwal_minum_obat');
        res.json(jadwal)
    } catch (e) {
        console.log(e);
    }
}

//== Masukan Data Rekam Medis Obat dan Tindakan Per Kunjungan
exports.createriwayatpasien = async function (req, res){
    let norm = req.body.norm
    let poli = req.body.poli
    let namadokter = req.body.namadokter
    let obatpasien = req.body.obatpasien
    let icd = req.body.icd
    let tindakan = req.body.tindakan
    let kodefaskes = req.body.kodefaskes

    let d = new Date(),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;
    
    let  idkunjungan = norm+year+month+day
    let i = 0
    let j = 0
    let k = 0
    try{
       
        let insertkunjungan = await connection('table_kunjungan_pasien').insert({
            "no_rm" : norm,
            "id_kunjungan" : idkunjungan,
            "poli" : poli,
            "dokter" : namadokter,
            "keluhan" : '',
            "id_faskes" : kodefaskes
        })

        console.log("Sukses Input Kunjungan")
        //insert data obat yang di berikan ke pasien
        for(i ;i<obatpasien.length; i++){
            let insertobat = await connection('table_obat_pasien').insert({
                "rm" : norm,
                "id_kunjungan" : idkunjungan,
                "nama_obat" : obatpasien[i].obat,
                "aturan" : obatpasien[i].jadwal,
                "catatan" : obatpasien[i].catatan
            })
        }
        console.log("Sukses Input Obat")
        console.log(icd)
        //insert data rm pasien
        for(j ;j<icd.length; j++){
            let inserticd = await connection('table_diagnosa_pasien').insert({
                "rm" : norm,
                "id_kunjungan" : idkunjungan,
                "icd" : icd[j].kodeicd,
                "nama_penyakit" : icd[j].rm,
                "id_faskes" : kodefaskes
            })
        }
        console.log("Sukses Input ICD")
        console.log(tindakan)
         //insert data tindakan dokter
         for(k ;k<tindakan.length; k++){
            let insertindakan = await connection('table_tindakan_dokter').insert({
                "rm" : norm,
                "id_kunjungan" : idkunjungan,
                "tindakan" : tindakan[k].tindakan,
            })
        }
        console.log("Sukses tidakan dokter")
        res.json({ 'status': '1'})
    } catch (e){

    }
}

// ambil data rekam medis berdasarkan kunjungan

exports.riwayatrekammedis = async function(req, res){
    let rm = req.params.id
    let riwayat = []
    let idkunjungan = await connection('table_kunjungan_pasien').where('no_rm', rm).groupBy('id_kunjungan')
    let i = 0

    for (i; i<idkunjungan.length; i++){
        let kunjungan = await connection('table_kunjungan_pasien').where('id_kunjungan', idkunjungan[i].id_kunjungan)
        let faskes = await connection('table_faskes_database').where('id_faskes', idkunjungan[i].id_faskes)
        let datadiagnosa = await connection('table_diagnosa_pasien').where('id_kunjungan', idkunjungan[i].id_kunjungan)
        let dataobat = await connection('table_obat_pasien').where('id_kunjungan', idkunjungan[i].id_kunjungan)
        let datatindakan = await connection('table_tindakan_dokter').where('id_kunjungan', idkunjungan[i].id_kunjungan)
        riwayat.push({kunjungan,faskes, datadiagnosa, dataobat, datatindakan})
    }
    res.json(riwayat)
}
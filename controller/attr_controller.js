'use strict';

var connection = require('./../config/db');

exports.attr = async  function(req,res){
    let agama = await connection('table_agama').select('*')
    let pekerjaan =  await connection('table_pekerjaan').select('*')
    let statuskeluarga = await connection('table_status_keluarga').select('*')

    res.json({
        'agama' : agama,
        'pekerjaan' : pekerjaan,
        'statuskeluarga' : statuskeluarga,
    })
}
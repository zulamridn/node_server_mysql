'use strict';
const bcrypt = require('bcrypt');

var response = require('./../config/response');
var connection = require('./../config/db');

async function getlastfaskesId(kabkotaid){
    let last = await connection('table_faskes_database').where('kabupaten_kota', kabkotaid).orderBy('id', 'desc').limit(1)
    if (last.length !== 0) {
        let faskesd = last[0].id_faskes
        let faskesautoinc = faskesd.substr(6, 9);
        console.log(faskesautoinc);
        let nomor = parseInt(faskesautoinc) + 1;
        if (nomor < 10) {
            let fixnomor = '000' + nomor;
           return fixnomor
        } else if (nomor >= 10 && nomor < 100) {
            let fixnomor = '00' + nomor;
           return fixnomor
        } else if(nomor >= 100 && nomor < 1000){
            let fixnomor = '0' + nomor;
            return fixnomor
        }else{
           let fixnomor = nomor;
           return fixnomor
        }
    } else {
       let  fixnomor = '0001';
       return fixnomor
    }
}

exports.createfaskes = async function (req, res){
    let namafaskes = req.body.namafaskes;
    let emailfaskes = req.body.emailfaskes;
    let nohpfaskes = req.body.nohpfaskes;
    let localapifaskes = req.body.localapifaskes;
    let alamatfaskes = req.body.alamatfaskes;
    let onesignalid = req.body.onesignalid;
    let onesignalsec = req.body.onesignalsec;
    let kabkotafaskes = req.body.kabkotafaskes;
    let provinsifaskes = req.body.provinsifaskes;
    let passwordfaskes = bcrypt.hashSync(req.body.passwordfaskes, 10)

    let konstanta ='1';
    let lastId = await getlastfaskesId(kabkotafaskes)
    let id = kabkotafaskes+konstanta+lastId

    try{
        let insert = await connection('table_faskes_database').insert({
            "id_faskes" : id,
            "nama_faskes" : namafaskes,     
            "email" : emailfaskes,
            "no_hp" : nohpfaskes,
            "local_api" : localapifaskes,
            "alamat" : alamatfaskes,
            "kabupaten_kota" : kabkotafaskes,
            "provinsi" : provinsifaskes,
            "password" : passwordfaskes,
        })
        res.json({ "status": "sukses" })
    }catch(e){
      console.log(e)
    }
}

exports.loginfaskes = async function(req, res){
    try{
        let cekId = await connection('table_faskes_database').where('id_faskes', req.body.faskesid)
        let password = await connection('table_faskes_database').where('id_faskes', req.body.faskesid)
        console.log(cekId.length)
        if(cekId.length > 0){
            if(bcrypt.compareSync(req.body.passwordfaskes, password[0].password)) {
                    res.json({"status" : '1', "data" : cekId});
               } else {
                    res.json({"status" : '2'});
               }
        }else{
            res.json({"status" : '3'});
        }
    }catch(e){
        console.log(e)
    }
}

exports.faskesbyid = async function (req, res) {
    try {
        let faskesbyid = await connection('table_faskes_database').where('id_faskes', req.params.id)
        res.json(faskesbyid)
    } catch (e) {
        console.log(e)
    }
}

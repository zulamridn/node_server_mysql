var express = require('express'),
    app = express(),
    port = process.env.port || 3000,
    bodyParser = require('body-parser')
    pasien_controller = require('./controller/pasien_controller');
var cors = require('cors')

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());

var routes = require('./routes/route');
routes(app);

app.listen(port);
console.log('Server Running')


'use strict';

module.exports = function(app){
    var pasien_controller = require('./../controller/pasien_controller')
    var faskes_controller = require('./../controller/faskes_controller')
    var attr_controller = require('./../controller/attr_controller')

    app.route('/')
        .get(pasien_controller.index);

    app.route('/pasien')
        .get(pasien_controller.users);

    app.route('/createpasien')
        .post(pasien_controller.pasiencreate);
    
    app.route('/pasienbyid/:id')
        .get(pasien_controller.userbyid)

    app.route('/createriwayatpasien')
        .post(pasien_controller.createriwayatpasien)

    app.route('/getrm/:id')
        .get(pasien_controller.riwayatrekammedis)

    app.route('/icd')
        .get(pasien_controller.icd)

    app.route('/obat')
        .get(pasien_controller.obat)
    
    app.route('/jadwal')
        .get(pasien_controller.jadwal)
    
    app.route('/createfaskes')
        .post(faskes_controller.createfaskes)
    
    app.route('/loginfaskes')
        .post(faskes_controller.loginfaskes)
    
    app.route('/faskesbyid/:id')
        .get(faskes_controller.faskesbyid)

    app.route('/attribut')
        .get(attr_controller.attr)

}